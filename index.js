const express = require("express");
const app = express();
const mongoose = require("mongoose");
require("dotenv").config();
const cors = require("cors");

// Import routes
const authRoute = require("./router/auth");
const loadRoute = require("./router/load");
const truckRoute = require("./router/truck");
const userRoute = require("./router/user");

app.use(express.json());
app.use(cors());
app.use((req, res, next) => {
  console.log(req.method, req.originalUrl);
  next();
});

mongoose.connect(
  process.env.DATABASE_URL,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  () => {
    console.log("connected to database");
  }
);

app.use("/api/auth", authRoute);
app.use("/api/loads", loadRoute);
app.use("/api/trucks", truckRoute);
app.use("/api/users", userRoute);

app.listen(process.env.PORT, () => {
  console.log("server has started");
});
