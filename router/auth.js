const router = require("express").Router();
const jwt = require("jsonwebtoken");
const User = require("../models/User");
const bcrypt = require("bcryptjs");
const Joi = require("joi");

const schema = Joi.object({
  email: Joi.string().email(),
  password: Joi.string().required().min(6),
  role: Joi.string().default(null),
});

router.post("/register", async (req, res) => {
  const { email, password, role } = req.body;

  // Validation;
  const { error } = schema.validate(req.body);

  if (error)
    res.status(400).json({ message: error.details[0].message.toString() });
  try {
    const nameExist = await User.findOne({ email });
    if (nameExist) {
      return res.status(401).json({ message: "email already exists" });
    }

    // Hash password
    const hashPassword = await bcrypt.hash(password, 10);
    // Create a new user
    const user = new User({
      email,
      password: hashPassword,
      role,
    });

    await user.save();
    res.status(200).json({ message: "Profile created successfully" });
  } catch (err) {
    res.status(400).json({ message: "string" });
  }
});

router.post("/login", async (req, res) => {
  const { email, password } = req.body;

  try {
    const user = await User.findOne({ email });

    if (!user) {
      return res.status(401).json({ message: "Email doesnt exists" });
    }

    // Validation password
    const validPassword = await bcrypt.compare(password, user.password);
    if (!validPassword) {
      return res.status(400).json({ message: "Invalid password" });
    }

    // JWT
    const token = await jwt.sign({ _id: user._id }, process.env.SECRET);

    res.status(200).header("Authorization", token).json({ jwt_token: token });
  } catch (err) {
    res.status(400).json({ message: "string" });
  }
});

module.exports = router;
