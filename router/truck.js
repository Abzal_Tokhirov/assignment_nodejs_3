const router = require("express").Router();
const verify = require("../router/verifyToken");
const Truck = require("../models/Truck");
const User = require("../models/User");

router.post("/", verify, async (req, res) => {
  const { type } = req.body;

  try {
    const user = await User.findById({ _id: req.user._id });

    if (user.role.toLowerCase() == "shipper") {
      return res.status(400).json({ message: "string" });
    }
    const usersTruckType = await Truck.find({ created_by: req.user._id });

    for (let i = 0; i < usersTruckType.length; i++) {
      if (usersTruckType[i].type === type) {
        return res.status(400).json({ message: "string" });
      }
    }
    const truck = new Truck({
      type,
      created_by: req.user._id,
    });

    await truck.save();
    res.status(200).json({ message: "Truck created successfully" });
  } catch (err) {
    res.status(400).json({ message: "string" });
  }
});

router.get("/", verify, async (req, res) => {
  try {
    const user = await User.findById({ _id: req.user._id });

    if (user.role.toLowerCase() == "shipper") {
      return res.status(400).json({ message: "string" });
    }
    const usersTruck = await Truck.find({ created_by: req.user._id });
    res.status(200).json({ trucks: usersTruck });
  } catch (err) {
    res.status(400).json({ message: "string" });
  }
});

router.get("/:id", verify, async (req, res) => {
  const { id } = req.params;

  try {
    const user = await User.findById({ _id: req.user._id });

    if (user.role.toLowerCase() == "shipper") {
      return res.status(400).json({ message: "string" });
    }
    const truckID = await Truck.findById({ _id: id });

    res.status(200).json({
      truck: {
        _id: truckID._id,
        created_by: truckID.created_by,
        assigned_to: truckID.assigned_to,
        type: truckID.type,
        status: truckID.status,
        created_date: truckID.created_date,
      },
    });
  } catch (err) {
    res.status(400).json({ message: "string" });
  }
});

router.put("/:id", verify, async (req, res) => {
  const { id } = req.params;
  const { type } = req.body;

  try {
    const user = await User.findById({ _id: req.user._id });

    if (user.role.toLowerCase() == "shipper") {
      return res.status(400).json({ message: "string" });
    }
    const truck = await Truck.findOne({ _id: id });

    await Truck.updateOne(truck, { type });
    res.status(200).json({ message: "Truck details changed successfully" });
  } catch (err) {
    res.status(400).json({ message: "string" });
  }
});

router.delete("/:id", verify, async (req, res) => {
  const { id } = req.params;

  try {
    const user = await User.findById({ _id: req.user._id });

    if (user.role.toLowerCase() == "shipper") {
      return res.status(400).json({ message: "string" });
    }
    const truckID = await Truck.findByIdAndRemove({ _id: id });
    res.status(200).json({ message: "Success" });
  } catch (err) {
    res.status(400).json({ message: "string" });
  }
});

router.post("/:id/assign", verify, async (req, res) => {
  const { id } = req.params;

  try {
    const user = await User.findById({ _id: req.user._id });

    if (user.role.toLowerCase() == "shipper") {
      return res.status(400).json({ message: "string" });
    }
    const truck = await Truck.findOneAndUpdate(
      { _id: id },
      { $set: { assigned_to: req.user._id } }
    );
    res.status(200).json(truck);
  } catch (err) {
    console.log(err);
    res.status(400).json({ message: "string" });
  }
});

module.exports = router;
