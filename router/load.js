const router = require("express").Router();
const verify = require("../router/verifyToken");
const User = require("../models/User");
const Load = require("../models/Load");
const Truck = require("../models/Truck");

router.get("/", verify, async (req, res) => {
  try {
    const reqQuery = req.query;
    const user = await User.findById({ _id: req.user._id });

    if (user.role.toLowerCase() == "shipper") {
      const shipperLoads = await Load.find({ created_by: req.user._id });

      return res.json({
        loads: shipperLoads,
      });
    }
    if (user.role.toLowerCase() == "driver") {
      const driverLoad = await Load.find({ assigned_to: req.user._id });
      console.log(driverLoad);
      return res.json({ loads: driverLoad });
    }
  } catch (err) {
    res.status(400).json({ message: "string" });
  }
});

router.post("/", verify, async (req, res) => {
  const postLoad = req.body;
  try {
    const user = await User.findById({ _id: req.user._id });
    if (user.role.toLowerCase() == "shipper") {
      const newLoad = new Load({
        created_by: req.user._id,
        assigned_to: postLoad.assigned_to,
        name: postLoad.name,
        status: postLoad.status,
        state: postLoad.state,
        payload: postLoad.payload,
        pickup_address: postLoad.pickup_address,
        delivery_address: postLoad.delivery_address,
        dimensions: postLoad.dimensions,
        logs: postLoad.logs,
        created_date: postLoad.created_date,
      });
      await newLoad.save();
      res.status(200).json({ message: "Load created successfully" });
    } else {
      return res.status(400).json({ message: "string" });
    }
  } catch (err) {
    res.json({ message: "string" });
  }
});

router.get("/active", verify, async (req, res) => {
  try {
    const user = await User.findById({ _id: req.user._id });

    if (user.role.toLowerCase() == "driver") {
      const activeDriverLoads = await Load.find({ assigned_to: req.user._id });

      res.status(200).json({ loads: activeDriverLoads });
    } else {
      res.status(400).json({ message: "string" });
    }
  } catch (err) {
    res.json({ message: "string" });
  }
});

router.patch("/active/state", verify, async (req, res) => {
  try {
    const user = await Load.findOneAndUpdate(
      { assigned_to: req.user._id },
      { $set: { state: "En route to Delivery" } }
    );
    res
      .status(200)
      .json({ message: "Load state changed to 'En route to Delivery'" });
  } catch (err) {
    res.status(400).json({ message: "string" });
  }
});

router.get("/:id", verify, async (req, res) => {
  const id = req.params.id;

  try {
    const user = await User.findById({ _id: req.user._id });
    const searchLoad = await Load.findById({ _id: id });

    res.status(200).json(searchLoad);
  } catch (err) {
    console.log(err);
    res.status(400).json({ message: "string" });
  }
});

router.put("/:id", verify, async (req, res) => {
  try {
    const user = await User.findById({ _id: req.user._id });
    const { name, payload, pickup_address, delivery_address, dimensions } =
      req.body;
    if (user.role.toLowerCase() == "driver") {
      return res.status(400).json({ message: "string" });
    }
    const load = await Load.findOneAndUpdate(
      {
        created_by: req.user._id,
        _id: req.params.id,
      },
      { $set: { name, payload, pickup_address, delivery_address, dimensions } }
    );
    console.log(load);
    res.json({ load });
  } catch (err) {
    res.json(err);
  }
});

router.delete("/:id", verify, async (req, res) => {
  const { id } = req.params;
  try {
    const user = await User.findById({ _id: req.user._id });
    if (user.role.toLowerCase() == "driver") {
      return res.status(400).json({ message: "string" });
    }

    const deletedLoad = await Load.findOneAndRemove({ _id: id });

    res.status(200).json({ message: "Load deleted successfully" });
  } catch (err) {
    console.log(err);
    res.status(400).json({ message: "string" });
  }
});

router.post("/:id/post", verify, async (req, res) => {
  try {
    const user = await User.findById({ _id: req.user._id });

    if (user.role.toLowerCase() == "driver") {
      return res.status(400).json({ message: "string" });
    }

    const searchLoad = await Load.findOneAndUpdate(
      { _id: req.params.id },
      { $set: { status: "POSTED" } }
    );
    res
      .status(200)
      .json({ message: "Load posted successfully", driver_found: true });
  } catch (err) {
    res.json({ message: "string" });
  }
});

router.get("/:id/shipping_info", verify, async (req, res) => {
  try {
    const user = await User.findById({ _id: req.user._id });
    if (user.role.toLowerCase() == "driver") {
      return res.status(400).json({ message: "string" });
    }

    const searchLoad = await Load.find({ created_by: req.user._id });
    res.status(200).json({ load: searchLoad });
  } catch (err) {}
});

module.exports = router;
