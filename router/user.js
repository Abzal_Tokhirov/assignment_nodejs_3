const router = require("express").Router();
const verify = require("../router/verifyToken");
const User = require("../models/User");
const bcrypt = require("bcryptjs");

router.get("/me", verify, async (req, res) => {
  try {
    const user = await User.findById({
      _id: req.user._id,
    });

    res.status(200).json({
      user: {
        _id: user._id,
        role: user.role,
        email: user.email,
        created_date: user.created_date,
      },
    });
  } catch (err) {
    res.status(400).json({ message: "string" });
  }
});

router.delete("/me", verify, async (req, res) => {
  try {
    await User.findByIdAndRemove({ _id: req.user._id });
    res.status(200).json({ message: "Profile deleted successfully" });
  } catch (err) {
    res.json({ message: "string" });
  }
});

router.patch("/me/password", verify, async (req, res) => {
  const { oldPassword, newPassword } = req.body;
  const user = await User.findById({ _id: req.user._id });
  const hashPassword = await bcrypt.hash(newPassword, 10);
  const validPassword = await bcrypt.compare(oldPassword, user.password);
  if (!validPassword) res.status(400).json({ message: "string" });

  try {
    await User.updateOne(user, { password: hashPassword });
    res.status(200).json({ message: "Success" });
  } catch (err) {
    console.log(err);
    res.status(400).json({ message: "string" });
  }
});

module.exports = router;
