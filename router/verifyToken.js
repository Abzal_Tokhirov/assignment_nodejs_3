const jwt = require("jsonwebtoken");

module.exports = async function (req, res, next) {
  const [, token] = req.headers["authorization"].split(" ");
  if (!token) {
    return res.status(401).json({ message: "Invalid token" });
  }

  try {
    const verified = await jwt.verify(token, process.env.SECRET);
    req.user = verified;
    next();
  } catch (err) {
    res.json(400).json({ message: err });
  }
};
