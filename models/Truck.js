const mongoose = require("mongoose");

const truckSchema = new mongoose.Schema({
  created_by: {
    type: String,
  },

  assigned_to: {
    type: String,
    default: null,
  },
  type: {
    type: String,
    default: null,
  },
  status: {
    type: String,
    default: "IS",
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = mongoose.model("Truck", truckSchema);
