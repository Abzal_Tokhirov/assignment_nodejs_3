const mongoose = require("mongoose");

const loadSchema = new mongoose.Schema({
  created_by: {
    type: String,
  },

  assigned_to: {
    type: String,
    default: null,
  },
  status: {
    type: String,
    default: "NEW",
  },
  state: {
    type: String,
    default: null,
  },
  name: {
    type: String,
  },
  payload: {
    type: Number,
  },
  pickup_address: {
    type: String,
  },
  delivery_address: {
    type: String,
  },
  dimensions: {
    width: {
      type: Number,
    },
    length: {
      type: Number,
    },
    height: {
      type: Number,
    },
  },
  logs: [
    {
      message: {
        type: String,
      },
      time: {
        type: Date,
        default: Date.now(),
      },
    },
  ],
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = mongoose.model("Load", loadSchema);
